# Character By Character Comparison

*To use this sample, please checkout, clone or download and unzip this sample repository into the samples directory of the XML-Compare release.*  

*The character-by-character folder should be located such that it is two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-14_1_0_j/samples/character-by-character`.*

## How to run a Character By Character Comparison

*In the following description please replace ```x.y.z``` by the version numbers of XML Compare you are using for example ```14.1.0```.* 

### Command Line Interface with DCP File

1. Copy the character-by-character directory to the /DeltaXML-XML-Compare-x_y_z_j/samples/ directory
2. Copy the doc-cbc-compare-xhtml.dcp file from that directory to the directory alongside deltaxml-x.y.z.jar
3. From the terminal run the following java commmand

    ```java -jar command-x.y.z.jar compare doc-cbc-xhtml samples/character-by-character/input1.html samples/character-by-character/input2.html output.html```


### Java API

The following Java snippet shows how you can enable Character By Character for a comparison using the `ResultReadabilityOptions`.

```java
    ResultReadabilityOptions resultOptions = new ResultReadabilityOptions();
    resultOptions.setCharacterByCharacterEnabled(true);

    // This is the example DCP file from this sample. You can create your own and name it as desired.
    File dcpfile= new File("doc-cbc-compare-xhtml.dcp");
    DCPConfiguration dcpcfg= new DCPConfiguration(dcpfile);
    dcpcfg.generate();
    DocumentComparator dc = dcpcfg.getDocumentComparator();

    // Here we set the result readability option on DocumentComparator
    dc.setResultReadabilityOptions(resultOptions);
    dc.compare(inputFileA, inputFileB, result);
```

### Enabling and Disabling per Element
When the character by character has been enabled on DocumentComparator, it may then be enabled or disabled at any point in the input XML by setting
an attribute `@character-by-character` to true or false respectively on any element.
For example the following input file would disable character by character comparison for this file.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<root xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1" deltaxml:character-by-character="false">some input</root>
```

### New Implementation Note
Previously Character By Character was a sample which used PipelinedComparator. It has now been deprecated.
Please contact us via support if you still want to use it.
